//THIS IS A SHITPOST
//made by emzi0767

public static IReadOnlyDictionary<TProp, T> CreateQueryWrapper<T, TProp>(
    this IReadOnlyDictionary<ulong, T> dict, 
    Expression<Func<T, TProp>> querySubject,
    IEqualityComparer<TProp> equalityComparer = default)
{
    if (!(querySubject.Body is MemberExpression member) || !(member.Member is PropertyInfo prop))
        throw new ArgumentException("Must be a property expression.", nameof(querySubject));

    var gm = prop.GetGetMethod();
    if (gm is null)
        throw new ArgumentException("Must be a readable property.", nameof(querySubject));

    var getter = gm.CreateDelegate(typeof(Func<T, TProp>)) as Func<T, TProp>;

    return new QueryWrapper<T, TProp>(dict.Values, getter, equalityComparer);
}

private class QueryWrapper<T, TProp> : IReadOnlyDictionary<TProp, T>
{
    private IEnumerable<T> Items { get; }
    private Func<T, TProp> Getter { get; }
    private IEqualityComparer<TProp> EqualityComparer { get; }

    public IEnumerable<TProp> Keys
        => this.Items.Select(this.Getter).Distinct(this.EqualityComparer);

    public IEnumerable<T> Values
        => this.Items;

    public int Count
        => this.Items.Count();

    public T this[TProp key]
    {
        get => this.Items.Single(x => this.EqualityComparer.Equals(this.Getter(x), key));
    }

    public QueryWrapper(
        IEnumerable<T> items,
        Func<T, TProp> getter, 
        IEqualityComparer<TProp> equalityComparer)
    {
        this.Items = items;
        this.Getter = getter;
        this.EqualityComparer = equalityComparer ?? EqualityComparer<TProp>.Default;
    }

    public bool ContainsKey(TProp key)
        => this.Items.Any(x => this.EqualityComparer.Equals(this.Getter(x), key));

    public bool TryGetValue(TProp key, out T value)
    {
        value = default;
        if (!this.ContainsKey(key))
            return false;

        value = this[key];
        return true;
    }

    public IEnumerator<KeyValuePair<TProp, T>> GetEnumerator()
        => this.Items.Select(x => new KeyValuePair<TProp, T>(this.Getter(x), x)).GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
        => this.GetEnumerator();
}

// Usage:
var qw = gld.Channels.CreateQueryWrapper(x => x.Name);
var chn = qw["my-channel"];